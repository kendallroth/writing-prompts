/** @type {import('vls').VeturConfig} */
module.exports = {
  // Override VS Code Vetur settings
  settings: {},
  // Support monorepo (Vetur cannot find 'web' directory otherwise)
  projects: [
    "./web",
  ],
};