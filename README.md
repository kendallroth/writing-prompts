# Writing Prompts

> **Writing Prompts** is a platform for sparking new ideas in aspiring writers!

## Architecture

### API

The API is architected with the [NestJS](https://docs.nestjs.com/) framework

- **[NestJS](https://docs.nestjs.com/)** - Framework for building modular server applications
- **[TypeScript](http://www.typescriptlang.org/)** - Typed superset of JavaScript for better documentation, code safety, and experience
- **[TypeORM](https://typeorm.io/)** - Powerful Object Relational Mapper for NodeJS

### Database

The database uses [PostgreSQL](https://www.postgresql.org/)

### Web

The web application is an SPA built with [VueJS](https://vuejs.org/)

- **[VueJS](https://vuejs.org/)** - Model-View framework for building user interfaces and SPAs

## Setup

Setup information has been moved to the appropriate `README` files.

- [API setup](./api/README.md)
- [Database setup](./database/README.md)
- [Web setup](./web/README.md)

However, a quickstart for returning developers is as follows:

1. Install dependencies in `api` and `web` directories
2. Copy example environment variable files and populate with valid values
3. Start API and database Docker containers with `docker-compose up`
4. Start web process with `npm run start` in `web` directory
