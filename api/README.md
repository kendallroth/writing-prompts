# Writing Prompts API

## Setup

1. Install API dependencies locally (_for editor type completion_)
2. Copy example environment variable files and populate with valid values
3. Start API (and database) Docker containers with `docker-compose up`
4. Install API dependencies in container
5. Run migrations in container
6. Access API at `localhost:3001`

```bash
# Install dependencies locally
$ npm install

# Connect to Docker container
$ docker-compose exec writers-api

# Install dependencies in container
> npm install
# Run database migrations
> npm run migrate
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
