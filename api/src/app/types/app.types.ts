/** Api info */
export interface IApiInfo {
  /** Version release date */
  releaseDate?: string;
  /** Git commit hash */
  releaseHash?: string;
  /** API version */
  version: string;
}
