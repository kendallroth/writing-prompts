import { Controller, Get, HttpStatus } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";

// Utilities
import { AppService } from "./app.service";

// Types
import { IApiInfo } from "./types";

/** App controller */
@ApiTags("app")
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /** Basic API information */
  @Get("/")
  @ApiOperation({ summary: "Basic API information" })
  @ApiResponse({ description: "API information", status: HttpStatus.OK })
  getInfo(): IApiInfo {
    return this.appService.getInfo();
  }

  /** Health check route */
  @Get("/health")
  @ApiOperation({ summary: "API health check" })
  @ApiResponse({ status: HttpStatus.OK })
  getHealth(): void {
    return;
  }
}
