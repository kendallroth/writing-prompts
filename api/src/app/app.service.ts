import { Inject, Injectable } from "@nestjs/common";
import { ConfigType } from "@nestjs/config";

// Utilities
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require("../../package.json");
import { appConfig as _appConfig } from "@/config";

// Types
import { IApiInfo } from "./types";

@Injectable()
export class AppService {
  constructor(
    @Inject(_appConfig.KEY)
    private readonly appConfig: ConfigType<typeof _appConfig>,
  ) {}

  getInfo(): IApiInfo {
    return {
      releaseDate: this.appConfig.releaseDate,
      releaseHash: this.appConfig.releaseHash,
      version,
    };
  }
}
