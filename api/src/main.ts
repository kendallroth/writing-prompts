import { ValidationPipe } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { HttpAdapterHost, NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";

// Utilities
import { UncaughtExceptionFilter } from "@common/filters";
import { AppModule } from "./app/app.module";

/** Bootstrap the NestJS application */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const configService = app.get(ConfigService);

  // Globally transform payload objects to match their TypeScript definition
  // Source: https://docs.nestjs.com/techniques/validation#transform-payload-objects
  app.useGlobalPipes(
    new ValidationPipe({
      // Prevent showing default error messages
      // dismissDefaultMessages: true,
      transform: true,
    }),
  );

  // Exception filter to log uncaught exceptions in LogDNA
  // Source: https://docs.nestjs.com/exception-filters#inheritance
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new UncaughtExceptionFilter(httpAdapter));

  // Configure Swagger documentation
  // Source: https://docs.nestjs.com/openapi/introduction
  const swaggerConfig = new DocumentBuilder()
    .setTitle("Writing Prompts")
    .setDescription("API for Writing Prompts platform")
    .setVersion("1.0")
    .build();
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup("api", app, swaggerDocument, {
    swaggerOptions: { supportedSubmitMethods: [] },
  });

  const port = configService.get<number>("app.port", 3001);
  await app.listen(port);

  // eslint-disable-next-line no-console
  console.log(`Listening on port ${port}`);
}

bootstrap();
