import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";

// Utilities
import { JwtAuthGuard } from "./jwt-auth.guard";

/**
 * Ensure routes are protected with JWT tokens belonging to verified users
 */
@Injectable()
export class VerifiedUserGuard extends JwtAuthGuard {
  handleRequest(err: any, user: any) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }

    // Users must be verified to access most routes in the API
    if (!user.verifiedAt) {
      throw new ForbiddenException("User is not verified");
    }

    return user;
  }
}
