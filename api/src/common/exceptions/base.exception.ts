import { HttpException, HttpStatus } from "@nestjs/common";

/** Internal API error with code for app (400) */
export class CodedError extends HttpException {
  /** Error code string (useful with error map in web app) */
  code: string;

  /**
   * Internal error wrapper
   *
   * @param {string} code    - Error code string
   * @param {string} message - Error message
   * @param {number} status  - HTTP status
   */
  constructor(
    code: string,
    message?: string,
    status: number = HttpStatus.BAD_REQUEST,
  ) {
    super(
      {
        code,
        message,
        statusCode: status,
      },
      status,
    );

    this.code = code;
  }
}

/** Internal API error with throttling information (429) */
export class ThrottleError extends HttpException {
  /** Time to wait unil throttle ends */
  wait: number;

  /**
   * Internal error wrapper
   *
   * @param {string} message - Error message
   * @param {number} wait    - Time to wait until throttle ends
   */
  constructor(message: string, wait: number) {
    super(
      {
        message,
        statusCode: HttpStatus.TOO_MANY_REQUESTS,
        wait,
      },
      HttpStatus.TOO_MANY_REQUESTS,
    );

    this.wait = wait;
  }
}
