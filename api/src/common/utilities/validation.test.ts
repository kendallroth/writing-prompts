// Utilities
import { PASSWORD_REGEX, USERNAME_REGEX } from "./validation";

describe("Password validation", () => {
  it("accepts valid passwords", () => {
    const input = [
      "Passw@rd", // Pass - With inner special character
      "Pa5sw0rd!", // Pass - With numbers
      "Password!", // Pass - Without numbers
      "P@ssword!", // Pass - With multiple special characters
      "{assword!", // Pass - Start/end with special characters
      "Password!ThatIsQuiteL*ngAnd$ecure!sh", // Pass - Long password
    ];

    const result = input.map((i) => PASSWORD_REGEX.test(i));

    const expected = input.map(() => true);
    expect(result).toEqual(expected);
  });

  it("rejects invalid passwords", () => {
    const input = [
      "password", // Fail - No special characters
      "Passw0rd", // Fail - No special characters
      "paswrd!", // Fail - Too short
      "pasword", // Fail - Too short
    ];

    const result = input.map((i) => PASSWORD_REGEX.test(i));

    const expected = input.map(() => false);
    expect(result).toEqual(expected);
  });
});

describe("Username validation", () => {
  it("accepts valid usernames", () => {
    const input = [
      "u5ernam3", // Pass - With numbers
      "username", // Pass - Without numbers
      "2sernam3", // Pass - Start/end with numbers
    ];

    const result = input.map((i) => USERNAME_REGEX.test(i));

    const expected = input.map(() => true);
    expect(result).toEqual(expected);
  });

  it("rejects invalid usernames", () => {
    const input = [
      "usern@me", // Fail - Special characters
      " sernam ", // Fail - Space before/after
    ];

    const result = input.map((i) => USERNAME_REGEX.test(i));

    const expected = input.map(() => false);
    expect(result).toEqual(expected);
  });
});
