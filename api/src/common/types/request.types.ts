// Utilities
import { User } from "@modules/user/entities";

/** Authenticated API request */
export interface AuthenticatedRequest extends Request {
  /** Authenticated user (from JWT) */
  user: User;
}
