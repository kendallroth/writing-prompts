/** Forgot password response */
export interface IForgotPasswordResponse {
  /** Password reset code expiry time */
  expiry: number;
}
