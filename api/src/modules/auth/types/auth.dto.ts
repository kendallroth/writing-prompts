import { IsNotEmpty, IsString, Matches, MinLength } from "class-validator";

// Utilities
import { PASSWORD_REGEX } from "@common/utilities";

/** Authentication login request */
export class AuthLoginDto {
  /** User password */
  @IsString()
  @IsNotEmpty()
  password!: string;

  /** User platform username */
  @IsString()
  @IsNotEmpty()
  username!: string;
}

/** Change authenticated user password */
export class ChangePasswordDto {
  // NOTE: Current password only needs to match DB password (in case not valid with regex...)

  /** Old user password */
  @IsString()
  @IsNotEmpty()
  oldPassword!: string;

  /** New user password */
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @Matches(PASSWORD_REGEX, {
    message: "Password is invalid",
  })
  newPassword!: string;
}

/** Use a refresh token to generate a new auth token */
export class RefreshTokenDto {
  /** Refresh token from previous authentication workflow */
  @IsNotEmpty()
  @IsString({ message: "Refresh token is required" })
  refreshToken!: string;

  /** User ID for refresh token */
  @IsNotEmpty()
  @IsString({ message: "User ID is required" })
  userId!: string;
}
