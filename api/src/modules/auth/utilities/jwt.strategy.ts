import { ExtractJwt, Strategy } from "passport-jwt";
import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { ConfigType } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";

// Utilities
import { UserService } from "@modules/user/services";
import { jwtConfig as _jwtConfig } from "../config";
import { IJwtValidation } from "../types";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(_jwtConfig.KEY)
    private readonly jwtConfig: ConfigType<typeof _jwtConfig>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConfig.jwtSecret,
      usernameField: "username",
    });
  }

  /**
   * Respond to a validated JWT token with the authenticated user
   *
   * @param   payload - JWT validation payload
   * @returns Authenticated user (will be attached to request object)
   */
  async validate(payload: IJwtValidation) {
    // TODO: Determine if any enrichment should be provided to user object!
    return this.userService.findByUsername(payload.username);
  }
}
