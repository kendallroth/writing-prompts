import {
  Body,
  Controller,
  Request,
  Post,
  UseGuards,
  Patch,
} from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";

// Utilities
import { JwtAuthGuard } from "@common/guards";
import {
  AuthService,
  ForgotPasswordService,
  RefreshTokenService,
} from "../services";

// Types
import { AuthenticatedRequest } from "@common/types";
import {
  AuthLoginDto,
  ChangePasswordDto,
  ForgotPasswordRequestDto,
  ForgotPasswordResetDto,
  IForgotPasswordResponse,
  IAuthenticationResponse,
  RefreshTokenDto,
} from "../types";

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly forgotPasswordService: ForgotPasswordService,
    private readonly refreshTokenService: RefreshTokenService,
  ) {}

  /** Authentication login */
  @ApiOperation({ summary: "Authentication login" })
  @Post("/login")
  async authLogin(
    @Body() payload: AuthLoginDto,
  ): Promise<IAuthenticationResponse> {
    return this.authService.login(payload);
  }

  /** Change authenticated user's password */
  @ApiOperation({ summary: "Change user's password" })
  @UseGuards(JwtAuthGuard)
  @Patch("/password/change")
  async changePassword(
    @Body() payload: ChangePasswordDto,
    @Request() req: AuthenticatedRequest,
  ): Promise<void> {
    return this.authService.changePassword(req.user, payload);
  }

  /** Forgot password request */
  @ApiOperation({ summary: "Request a forgotten password reset" })
  @Post("/password/forget")
  async forgotPasswordRequest(
    @Body() payload: ForgotPasswordRequestDto,
  ): Promise<IForgotPasswordResponse> {
    return this.forgotPasswordService.forgotPasswordRequest(payload);
  }

  /** Forgot password reset (after email code verification) */
  @ApiOperation({ summary: "Reset a forgotten password (via email code)" })
  @Post("/password/reset")
  async forgotPasswordReset(
    @Body() payload: ForgotPasswordResetDto,
  ): Promise<void> {
    return this.forgotPasswordService.forgotPasswordReset(payload);
  }

  /** Refresh user's auth token using refresh token */
  @ApiOperation({ summary: "Refresh JWT authentication tokens" })
  @Post("/refresh-token")
  async refreshToken(
    @Body() payload: RefreshTokenDto,
  ): Promise<IAuthenticationResponse> {
    return this.refreshTokenService.refreshAuthToken(payload);
  }
}
