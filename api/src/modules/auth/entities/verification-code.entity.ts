import { Entity, ManyToOne, Column, JoinColumn } from "typeorm";

// Models
import { UsableTokenEntity } from "@common/entities";
import { User } from "@modules/user/entities";

// Types
import { VerificationCodeType } from "../types";

@Entity({ name: "verification_code" })
export class VerificationCode extends UsableTokenEntity {
  /** Verification code */
  @Column("text", { unique: true })
  public code!: string;

  /** Verification code type */
  @Column({
    type: "enum",
    enum: VerificationCodeType,
  })
  public type!: VerificationCodeType;

  @Column("uuid", { name: "user_id" })
  public userId!: string;

  /** Verification code owner */
  @ManyToOne(() => User, (user: User) => user.verificationCodes)
  @JoinColumn({ name: "user_id" })
  public user!: User;
}
