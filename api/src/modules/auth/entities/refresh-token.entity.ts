import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";

// Types
import { UsableTokenEntity } from "@common/entities";
import { User } from "@modules/user/entities";

@Entity({ name: "refresh_token" })
export class RefreshToken extends UsableTokenEntity {
  /**
   * Refresh token
   *
   * NOTE: Refresh tokens are hashed with user ID upon storage
   */
  @Column("text", { unique: true })
  token!: string;

  @Column("uuid", { name: "user_id" })
  public userId!: string;

  /** Refresh token owner */
  @ManyToOne(() => User, (user) => user.refreshTokens, { nullable: false })
  @JoinColumn({ name: "user_id" })
  user!: User;
}
