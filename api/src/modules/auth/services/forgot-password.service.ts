import { forwardRef, Inject, Injectable } from "@nestjs/common";

// Utilities
import { UserService } from "@modules/user/services";
import { AuthService } from "./auth.service";
import { codeExpiryLength, TokenService } from "./token.service";

// Types
import {
  VerificationCodeType,
  ForgotPasswordRequestDto,
  ForgotPasswordResetDto,
  IForgotPasswordResponse,
} from "../types";
import { ThrottleError } from "@common/exceptions";

@Injectable()
export class ForgotPasswordService {
  constructor(
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  /**
   * Request a password reset
   *
   * @param   credentials - User credentials (email)
   * @returns Password reset information
   */
  public async forgotPasswordRequest(
    credentials: ForgotPasswordRequestDto,
  ): Promise<IForgotPasswordResponse> {
    const { email } = credentials;

    const verificationType = VerificationCodeType.PASSWORD_RESET;
    const codeExpiry = codeExpiryLength[verificationType].expiry;

    // Invalid email should not inform user that there is no account with this email,
    //   which means "faking" a password reset (but no code is ever generated/sent).
    const user = await this.userService.findByEmail(email);
    if (!user) {
      return {
        expiry: codeExpiry,
      };
    }

    // TODO: Check if there are existing password reset codes (disable) and prevent resending too quickly.
    //         This should maybe be done in a separate "resend" workflow and utilize the "regenerateVerificationCode" function.
    const codeThrottle = await this.tokenService.checkCodeThrottling(
      user,
      VerificationCodeType.PASSWORD_RESET,
    );
    if (!codeThrottle.valid) {
      throw new ThrottleError("Wait before requesting again", codeThrottle.delay); // prettier-ignore
    }

    // Create password reset verification code and send to email
    const verificationCode = await this.tokenService.createVerificationCode(
      user,
      verificationType,
    );

    // TODO: Send emails with SendGrid
    console.log(`[PasswordReset]: Use '${verificationCode.code}' to reset password`); // prettier-ignore

    return {
      expiry: codeExpiry,
    };
  }

  /**
   * Reset a user password with emailed code
   *
   * @param  credentials - Password reset credentials
   * @throws Error if user attempts to set password to last password
   */
  public async forgotPasswordReset(
    credentials: ForgotPasswordResetDto,
  ): Promise<void> {
    const { code, password } = credentials;

    // TODO: Must check password validity BEFORE marking verification code as used!

    // Validate and use a verification code (throws errors if invalid)
    const user = await this.tokenService.useVerificationCode(
      code,
      VerificationCodeType.PASSWORD_RESET,
    );

    // NOTE: Throws error if user attempts to change password to last password!
    await this.authService.setPassword(user, password);
  }
}
