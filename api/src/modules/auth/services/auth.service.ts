import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";
import { ConfigType } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";

// Utilities
import { CodedError } from "@common/exceptions";
import { User } from "@modules/user/entities";
import { UserService } from "@modules/user/services";
import { jwtConfig as _jwtConfig } from "../config";
import { PasswordService } from "./password.service";
import { RefreshTokenService } from "./refresh-token.service";

// Types
import {
  IAuthenticationResponse,
  ChangePasswordDto,
  AuthLoginDto,
} from "../types";

@Injectable()
export class AuthService {
  constructor(
    @Inject(_jwtConfig.KEY)
    private readonly jwtConfig: ConfigType<typeof _jwtConfig>,
    private readonly jwtService: JwtService,
    private readonly passwordService: PasswordService,
    @Inject(forwardRef(() => RefreshTokenService))
    private readonly refreshTokenService: RefreshTokenService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  /**
   * Change the authenticated user's password
   *
   * @param user                    - Authenticated user
   * @param credentials             - User password credentials
   * @param credentials.newPassword - User password credentials
   * @param credentials.oldPassword - User password credentials
   */
  async changePassword(
    user: User,
    credentials: ChangePasswordDto,
  ): Promise<void> {
    const { newPassword, oldPassword } = credentials;
    if (!newPassword) throw new BadRequestException("Password cannot be empty");

    const oldPasswordHash = await this.userService.getPasswordHash(user);

    // Old password must match user's last passwod
    const oldPasswordMatches = await this.passwordService.verify(
      oldPassword,
      oldPasswordHash,
    );
    if (!oldPasswordMatches) {
      throw new CodedError(
        "CHANGE_PASSWORD__WRONG_PASSWORD",
        "Incorrect old password",
      );
    }

    // Password cannot match user's last password
    const newPasswordMatchesOld = await this.passwordService.verify(
      newPassword,
      oldPasswordHash,
    );
    if (newPasswordMatchesOld) {
      throw new CodedError(
        "CHANGE_PASSWORD__PASSWORD_MATCHES_OLD",
        "Password cannot match last password",
      );
    }

    await this.userService.setPassword(user, newPassword);
  }

  /**
   * Generate a JWT auth token
   *
   * @param   user - Authorized user
   * @returns JWT auth token
   */
  generateAuthJwt(user: User): string {
    return this.jwtService.sign({ email: user.email, username: user.username });
  }

  /**
   * Login authentication using JWT workflow
   *
   * @param   credentials - User credentials
   * @returns Authentication JWT
   */
  public async login(
    credentials: AuthLoginDto,
  ): Promise<IAuthenticationResponse> {
    const { password, username } = credentials;

    // Invalid email should not inform user that there is no account with this username!
    const invalidCredentialsMessage = "Invalid authentication credentials";
    const user = await this.userService.findByUsername(username);
    if (!user) {
      throw new UnauthorizedException(invalidCredentialsMessage);
    }

    const passwordHash = await this.userService.getPasswordHash(user);
    const passwordMatches = await this.passwordService.verify(
      password,
      passwordHash,
    );
    if (!passwordMatches) {
      throw new UnauthorizedException(invalidCredentialsMessage);
    }

    // Track the last time a user signed in
    await this.userService.logSignIn(user);

    return this.createAuthTokens(user);
  }

  /**
   * Set a user's password
   *
   * NOTE: Requires additional verification to make sure user can change password!
   * NOTE: Password cannot match user's last password (throws error)
   *
   * @param  user     - User to change password for
   * @param  password - New user password
   * @throws Password cannot match user's last password
   */
  async setPassword(user: User, password: string): Promise<void> {
    if (!password) throw new BadRequestException("Password cannot be empty");

    // Password cannot match user's last password
    const oldPasswordHash = await this.userService.getPasswordHash(user);
    const matchesOldPassword = await this.passwordService.verify(
      password,
      oldPasswordHash,
    );
    if (matchesOldPassword) {
      throw new BadRequestException("Password cannot match last password");
    }

    await this.userService.setPassword(user, password);
  }

  /**
   * Create JWT tokens for an authenticated user
   *
   * @param   user - Authenticated user
   * @returns JWT tokens
   */
  public async createAuthTokens(user: User): Promise<IAuthenticationResponse> {
    const { jwtExpirySeconds } = this.jwtConfig;

    const token = this.generateAuthJwt(user);
    // Refresh token is hashed before storage, but plaintext refresh token must be sent to user!
    const refreshToken = await this.refreshTokenService.generateRefreshToken(
      user,
    );

    return {
      expiresIn: jwtExpirySeconds,
      refreshToken,
      token,
      userId: user.id,
    };
  }
}
