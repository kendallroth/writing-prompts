import { forwardRef, Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";

// Utilities
import { AuthModule } from "@modules/auth/auth.module";
import { User } from "./entities";
import { UserController } from "./controllers";
import { UserService } from "./services";

@Module({
  controllers: [UserController],
  exports: [UserService],
  imports: [
    TypeOrmModule.forFeature([User]),
    forwardRef(() => AuthModule),
    PassportModule.register({ defaultStrategy: "jwt" }),
  ],
  providers: [UserService],
})
export class UserModule {}
