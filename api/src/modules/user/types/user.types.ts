/** User's own profile (private) */
export interface IUserPrivateProfile {
  /** User ID */
  id: string;
  /** When user was created */
  createdAt: Date;
  /** User email */
  email: string;
  /** User display name (pen name) */
  name: string | null;
  /** User platform name */
  username: string;
  /** When user verified their account */
  verifiedAt?: Date | null;
}

/** User verification resend response */
export interface IUserVerifyResendResponse {
  /** Expiry time for verification email (seconds) */
  expiry: number;
}
