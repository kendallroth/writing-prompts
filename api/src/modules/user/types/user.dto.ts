import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
} from "class-validator";

// Utilities
import { PASSWORD_REGEX, USERNAME_REGEX } from "@common/utilities";

/** User registration DTO */
export class UserRegisterDto {
  /** User email */
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email!: string;

  /** User password */
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @Matches(PASSWORD_REGEX, {
    message: "Password is invalid",
  })
  password!: string;

  /** User platform username */
  @IsString()
  @IsNotEmpty()
  @MinLength(4)
  @Matches(USERNAME_REGEX, {
    message: "Username is invalid",
  })
  username!: string;
}

/** User verification DTO */
export class UserVerifyDto {
  /** Account verification code */
  @IsString()
  @IsNotEmpty()
  code!: string;
}

/** User verification email resend DTO */
export class UserVerifyResendDto {
  /** Unverified account email */
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email!: string;
}
