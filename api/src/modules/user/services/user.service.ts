import {
  BadRequestException,
  ConflictException,
  forwardRef,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { InjectRepository } from "@nestjs/typeorm";
import { ILike, Repository } from "typeorm";

// Utilities
import { CodedError, ThrottleError } from "@common/exceptions";
import {
  AuthService,
  codeExpiryLength,
  PasswordService,
  TokenService,
} from "@modules/auth/services";
import { User } from "../entities";

// Types
import {
  IAuthenticationResponse,
  VerificationCodeType,
} from "@modules/auth/types";
import {
  IUserPrivateProfile,
  IUserVerifyResendResponse,
  UserRegisterDto,
  UserVerifyDto,
  UserVerifyResendDto,
} from "../types";

// Minimum minutes between regenerating verification codes
const MIN_VERIFICATION_CODE_REGEN_TIME = 1 * 60;

@Injectable()
export class UserService {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
    @Inject(forwardRef(() => PasswordService))
    private readonly passwordService: PasswordService,
    private readonly tokenService: TokenService,
    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
  ) {}

  /**
   * Find a user by email
   *
   * @param   email - User email
   * @returns User with email address
   */
  async findByEmail(email: string): Promise<User | undefined> {
    return this.userRepo.findOne({ where: { email: ILike(email) } });
  }

  /**
   * Find a user by username
   *
   * @param   username - User username
   * @returns User with username
   */
  async findByUsername(username: string): Promise<User | undefined> {
    return this.userRepo.findOne({ where: { username: ILike(username) } });
  }

  /**
   * Get a password hash (for login verification)
   *
   * @param   user - User email
   * @returns User password hash
   */
  async getPasswordHash(user: User): Promise<string> {
    // Password hash is excluded from TypeORM query results (requires raw query)
    // NOTE: We know the user exists because a user entity was provided
    const hashResult = await this.userRepo.query(
      `SELECT password FROM "user" WHERE id = $1;`,
      [user.id],
    );
    if (!hashResult || !hashResult.length) return "";

    return hashResult[0].password || "";
  }

  /**
   * Get user's private profile information
   *
   * @param   user - Authenticated user
   * @returns User's private profile
   */
  async getPrivateProfile(user: User): Promise<IUserPrivateProfile> {
    return {
      id: user.id,
      createdAt: user.createdAt,
      email: user.email,
      name: user.name,
      username: user.username,
      verifiedAt: user.verifiedAt,
    };
  }

  /**
   * Log the last time a user signed in (helpful for support)
   *
   * @param   user - Authenticated user
   * @returns Authenticated user
   */
  async logSignIn(user: User): Promise<User> {
    user.lastLoginAt = new Date();

    return this.userRepo.save(user);
  }

  /**
   * Mark a user as verified
   *
   * @param   user - Unverified user
   * @returns Verified user
   */
  async markVerified(user: User): Promise<User> {
    // Avoid marking user as verified twice!
    if (user.verifiedAt) return user;

    user.verifiedAt = new Date();

    return this.userRepo.save(user);
  }

  /**
   * Register a new user
   *
   * @param   payload - New user credentials
   * @returns Authentication tokens
   */
  async registerUser(
    payload: UserRegisterDto,
  ): Promise<IAuthenticationResponse> {
    const { email: _email, password, username: _username } = payload;

    const email = _email.trim().toLowerCase();
    const username = _username.trim();
    const usernameLower = username.toLowerCase();

    const emailUser = await this.userRepo.findOne({ where: { email } });
    if (emailUser) {
      throw new CodedError(
        "REGISTER__EMAIL_ALREADY_USED",
        "Email is already registered",
        HttpStatus.CONFLICT,
      );
    }

    const usernameUser = await this.userRepo.findOne({
      where: { username: usernameLower },
    });
    if (usernameUser) {
      throw new CodedError(
        "REGISTER__USERNAME_ALREADY_USED",
        "Username is already registered",
        HttpStatus.CONFLICT,
      );
    }

    // TODO: Also enforce username uniqueness (maybe case insensitive?)

    const passwordHash = await this.passwordService.hash(password);

    const user = await this.userRepo.save({
      email,
      password: passwordHash,
      username,
      verifiedAt: null,
    });

    // Create verification code and send to email
    const verificationType = VerificationCodeType.ACCOUNT_VERIFICATION;
    const verificationCode = await this.tokenService.createVerificationCode(
      user,
      verificationType,
    );

    // TODO: Send emails with SendGrid
    const appUrl = this.configService.get<string>("app.webAppUrl");
    const verificationUrl = `${appUrl}/verify/${verificationCode.code}`;
    console.log(`[UserRegister]: Use '${verificationCode.code}' to verify user (${verificationUrl})`); // prettier-ignore

    return this.authService.createAuthTokens(user);
  }

  /**
   * Hash and set a user's password (no verification!)
   *
   * NOTE: Any verification must happen before setting password!
   *
   * @param  user     - User to change password for
   * @param  password - New user password (plaintext)
   */
  async setPassword(user: User, password: string): Promise<void> {
    if (!password) throw new BadRequestException("Password cannot be empty");

    const passwordHash = await this.passwordService.hash(password);
    user.password = passwordHash;

    await this.userRepo.save(user);
  }

  /**
   * Verify a newly registered user
   *
   * @param   payload - Account verification code
   * @returns Authentication tokens
   */
  async verifyUser(payload: UserVerifyDto): Promise<IAuthenticationResponse> {
    const { code } = payload;

    const verificationCode = await this.tokenService.getVerificationCode(
      code,
      VerificationCodeType.ACCOUNT_VERIFICATION,
    );
    if (!verificationCode) {
      throw new BadRequestException("Verification code not found");
    }

    // Users can only be verified once!
    if (verificationCode.user.verifiedAt) {
      throw new CodedError(
        "USER_VERIFY__ALREADY_VERIFIED",
        "User is already verified",
      );
    }

    // Validate and use a verification code (throws errors if invalid/missing)
    const user = await this.tokenService.useVerificationCode(
      code,
      VerificationCodeType.ACCOUNT_VERIFICATION,
    );

    await this.markVerified(user);

    // User may or may not already be signed in (should be able to replicate authentication if not)
    return this.authService.createAuthTokens(user);
  }

  /**
   * Resend a user verification email
   *
   * @param   payload - Unverified account email
   * @returns Email verification information
   */
  async verifyUserResend(
    payload: UserVerifyResendDto,
  ): Promise<IUserVerifyResendResponse> {
    const { email } = payload;

    const user = await this.findByEmail(email);
    if (!user) {
      throw new NotFoundException("User not found");
    }

    if (user.verifiedAt) {
      throw new ConflictException("User already verified");
    }

    const verificationType = VerificationCodeType.ACCOUNT_VERIFICATION;
    const codeExpiry = codeExpiryLength[verificationType].expiry;

    // Prevent resending email verification codes too rapidly
    const codeThrottle = await this.tokenService.checkCodeThrottling(
      user,
      verificationType,
      MIN_VERIFICATION_CODE_REGEN_TIME,
    );
    if (!codeThrottle.valid) {
      throw new ThrottleError("Wait before requesting again", codeThrottle.delay); // prettier-ignore
    }

    // Create email verification code and send to email
    const verificationCode = await this.tokenService.createVerificationCode(
      user,
      verificationType,
    );

    // TODO: Send emails with SendGrid
    const appUrl = this.configService.get<string>("app.webAppUrl");
    const verificationUrl = `${appUrl}/verify/${verificationCode.code}`;
    console.log(`[UserVerification]: Use '${verificationCode.code}' to verify user (${verificationUrl})`); // prettier-ignore

    return {
      expiry: codeExpiry,
    };
  }
}
