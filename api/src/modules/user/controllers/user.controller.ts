import {
  Body,
  Controller,
  Get,
  Patch,
  Post,
  Request,
  UseGuards,
} from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";

// Utilities
import { AuthenticatedRequest } from "@common/types";
import { JwtAuthGuard } from "@common/guards";
import { UserService } from "../services";

// Types
import { IAuthenticationResponse } from "@modules/auth/types";
import {
  IUserPrivateProfile,
  IUserVerifyResendResponse,
  UserRegisterDto,
  UserVerifyDto,
  UserVerifyResendDto,
} from "../types";

@ApiTags("user")
@Controller("user")
export class UserController {
  constructor(private readonly userService: UserService) {}

  /** Get current authenticated user profile */
  @ApiOperation({ summary: "Get authenticated user profile" })
  @UseGuards(JwtAuthGuard)
  @Get("/profile")
  async getProfile(
    @Request() req: AuthenticatedRequest,
  ): Promise<IUserPrivateProfile> {
    return this.userService.getPrivateProfile(req.user);
  }

  /** Register a new user */
  @ApiOperation({ summary: "Register a new user" })
  @Post("/register")
  async registerUser(
    @Body() payload: UserRegisterDto,
  ): Promise<IAuthenticationResponse> {
    return this.userService.registerUser(payload);
  }

  /** Verify a registered user */
  @ApiOperation({ summary: "Verify a registered user" })
  @Patch("/verify")
  async verifyUser(
    @Body() payload: UserVerifyDto,
  ): Promise<IAuthenticationResponse> {
    return this.userService.verifyUser(payload);
  }

  /** Resend user verification email */
  @ApiOperation({ summary: "Resend user verification email (via email code)" })
  @Post("/verify/resend")
  async verifyUserResend(
    @Body() payload: UserVerifyResendDto,
  ): Promise<IUserVerifyResendResponse> {
    return this.userService.verifyUserResend(payload);
  }
}
