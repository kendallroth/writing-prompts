import { Column, Entity, OneToMany } from "typeorm";

// Utilities
import { BaseEntity } from "@common/entities";
import { RefreshToken, VerificationCode } from "@modules/auth/entities";

@Entity({ name: "user" })
export class User extends BaseEntity {
  /** User email */
  @Column("text", { unique: true })
  email!: string;

  /** User platform username */
  @Column("text", { unique: true })
  username!: string;

  /** User display name (pen name) */
  @Column("text", { nullable: true })
  name!: string | null;

  /**
   * User password
   *
   * NOTE: Password should NEVER be selectable (even though just a hash)!
   */
  @Column("text", { select: false })
  password!: string;

  /** When user last signed in */
  @Column("timestamptz", { name: "last_login_at", nullable: true })
  lastLoginAt!: Date | null;

  /** When user was verified */
  @Column("timestamptz", { name: "verified_at", nullable: true })
  verifiedAt!: Date | null;

  /** Refresh tokens */
  @OneToMany(
    () => RefreshToken,
    (refreshToken: RefreshToken) => refreshToken.user,
  )
  refreshTokens!: RefreshToken[];

  /** Verification codes */
  @OneToMany(() => VerificationCode, (code: VerificationCode) => code.user)
  verificationCodes!: VerificationCode[];
}
