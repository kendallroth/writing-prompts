module.exports = {
  apps: [
    {
      name: "writers-api",
      script: "dist/src/main.js",
      error_file: "/dev/stderr",
      instances: 1,
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};